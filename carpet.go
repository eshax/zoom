package zoom

import (
	"errors"
	"fmt"
	"image"
	"image/draw"
	"log"
	"os"
	"sync"

	"gitee.com/eshax/images"
)

type carpet struct {
}

var Carpet = new(carpet)

/*
地毯式切图
	
*/
func (o *carpet) Run(source_path, target_path string) error {
	log.Println("地毯式切图", source_path, target_path)

	// 扩展名
	ext, err := CheckExtension(source_path)
	if err != nil {
		return err
	}

	cols, rows, err := Matrix.GetMatrix(target_path)
	if err != nil {
		return err
	}
	if cols == 0 || rows == 0 {
		return errors.New("cols or rows is zero")
	}
	log.Println("竖着滚", "cols:", cols, "rows:", rows)

	// 竖着滚
	wgr := sync.WaitGroup{}
	for col := 0; col < cols; col++ {
		wgr.Add(1)
		go func(target_path, ext string, col int) {
			for row := 0; row < rows; row++ {
				o.cutRow(target_path, ext, col, row)
			}
			wgr.Done()
		}(target_path, ext, col)
	}
	wgr.Wait()

	cols, rows, err = Matrix.GetMatrix(target_path)
	if err != nil {
		log.Println(err)
		return err
	}
	if cols == 0 || rows == 0 {
		return errors.New("cols or rows is zero")
	}
	log.Println("横着滚", "cols:", cols, "rows:", rows)

	// 横着滚
	wgc := sync.WaitGroup{}
	for row := 0; row < rows; row++ {
		wgc.Add(1)
		go func(target_path, ext string, row int) {
			for col := 0; col < cols; col++ {
				o.cutCol(target_path, ext, col, row)
			}
			wgc.Done()
		}(target_path, ext, row)
	}
	wgc.Wait()

	return nil
}

func (o *carpet) cutRow(target_path, ext string, col, row int) (err error) {
	imagePath := fmt.Sprintf("%s/%d_%d%s", target_path, col, row, ext)
	// log.Println(imagePath)
	_img, err := images.Load(imagePath)
	if err != nil {
		return err
	}
	img := _img.(*image.YCbCr)
	// log.Println(img.Bounds().Size())
	width, height := img.Bounds().Size().X, img.Bounds().Size().Y
	for top := (((row * height) / tileSize) * tileSize) - (row * height); top < height; top += tileSize {
		r := (row*height + top) / tileSize
		h := ((row*height + height) - (r * tileSize))
		if h > tileSize {
			h = tileSize
		}
		if top >= 0 {
			clip := img.SubImage(image.Rect(0, top, width, top+h)).(*image.YCbCr)
			if err := images.Save(fmt.Sprintf("%s/%d-%d%s", target_path, col, r, ext), clip); err != nil {
				return err
			}
		} else {
			clip := img.SubImage(image.Rect(0, 0, width, h+top)).(*image.YCbCr)
			_last, err := images.Load(fmt.Sprintf("%s/%d-%d%s", target_path, col, r, ext))
			if err != nil {
				return err
			}
			last := _last.(*image.YCbCr)
			rgba := image.NewRGBA(image.Rect(0, 0, last.Bounds().Size().X, last.Bounds().Size().Y+clip.Bounds().Size().Y))
			draw.Draw(rgba, rgba.Bounds(), last, last.Bounds().Min, draw.Over)
			draw.Draw(rgba, rgba.Bounds(), clip, clip.Bounds().Min.Sub(image.Pt(0, last.Bounds().Max.Y)), draw.Over)
			if err := images.Save(fmt.Sprintf("%s/%d-%d%s", target_path, col, r, ext), rgba); err != nil {
				return err
			}
		}
	}
	os.Remove(imagePath)
	return nil
}

func (o *carpet) cutCol(target_path, ext string, col, row int) (err error) {
	imagePath := fmt.Sprintf("%s/%d-%d%s", target_path, col, row, ext)
	_img, err := images.Load(imagePath)
	if err != nil {
		return err
	}
	img := _img.(*image.YCbCr)
	width, height := img.Bounds().Size().X, img.Bounds().Size().Y
	for left := (((col * width) / tileSize) * tileSize) - (col * width); left < width; left += tileSize {
		c := (col*width + left) / tileSize
		w := ((col*width + width) - (c * tileSize))
		if w > tileSize {
			w = tileSize
		}
		if left >= 0 {
			// 新建图片
			clip := img.SubImage(image.Rect(left, 0, left+w, height)).(*image.YCbCr)
			if err := images.Save(fmt.Sprintf("%s/%d_%d%s", target_path, c, row, ext), clip); err != nil {
				return err
			}
		} else {
			// 与已经生成的图像进行拼接
			clip := img.SubImage(image.Rect(0, 0, w+left, height)).(*image.YCbCr)
			_last, err := images.Load(fmt.Sprintf("%s/%d_%d%s", target_path, c, row, ext))
			if err != nil {
				return err
			}
			last := _last.(*image.YCbCr)
			rgba := image.NewRGBA(image.Rect(0, 0, last.Bounds().Size().X+clip.Bounds().Size().X, last.Bounds().Size().Y))
			draw.Draw(rgba, rgba.Bounds(), last, last.Bounds().Min, draw.Over)
			draw.Draw(rgba, rgba.Bounds(), clip, clip.Bounds().Min.Sub(image.Pt(last.Bounds().Max.X, 0)), draw.Over)
			if err := images.Save(fmt.Sprintf("%s/%d_%d%s", target_path, c, row, ext), rgba); err != nil {
				return err
			}
		}
	}
	os.Remove(imagePath)
	return nil
}
