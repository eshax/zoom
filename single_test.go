package zoom

import (
	"log"
	"testing"
)

func TestSingleRun(t *testing.T) {
	for i := 0; i < 10; i++ {
		if err := Single.Run("dist/0.jpg", "dist/0"); err != nil {
			t.Error(err)
		}
	}
	log.Println()
}
