package zoom

import (
	"log"
	"testing"
)

func TestRun(t *testing.T) {
	if err := Matrix.Run("dist/html/images/slice", "dist/html/images/slice-dzi"); err != nil {
		t.Error(err)
	}
}

func TestCheckExt(t *testing.T) {

	ext, err := CheckExtension("dist/images/100x")
	if err != nil {
		t.Error(err)
	}
	log.Println(ext)

	width, height, err := GetBlockSize("dist/images/100x", ext)
	if err != nil {
		t.Error(err)
	}
	log.Println(width, height)

	cols, rows, err := Matrix.GetMatrix("dist/html/images/100x-dzi/10")
	if err != nil {
		t.Error(err)
	}
	log.Println(cols, rows)

}

func TestJoin(t *testing.T) {
	sourcepath := "dist/lower"
	targetpath := "dist/lower/join"
	Matrix.Join(sourcepath, targetpath)
}
