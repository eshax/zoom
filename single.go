package zoom

import (
	"fmt"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
	"path"

	"gitee.com/eshax/images"
)

// 单图
type single struct {
}

var Single = new(single)

// 文件切图
func (o *single) Run(source, target string) error {

	os.RemoveAll(target)

	log.Println("文件切图", target)

	ext := path.Ext(source)

	img, err := images.Load(source)
	if err != nil {
		return err
	}

	width := img.Bounds().Max.X
	height := img.Bounds().Max.Y

	max_level := GetMaxLevel(width, height)

	for i := max_level; i > 0; i-- {
		levelPath := fmt.Sprintf("%s/%d", target, i)
		os.MkdirAll(levelPath, 0755)
		if i < max_level {
			img = images.ResizeImage(img, 0.5)
		}
		if err := Cut(img, levelPath, ext); err != nil {
			return err
		}
	}

	return nil
}
