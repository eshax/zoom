package zoom

import (
	"fmt"
	"image"
	"log"
	"math"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"

	"gitee.com/eshax/images"
	"gitee.com/eshax/ioex"
	"github.com/disintegration/imaging"

	"image/color"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

// 多图
type matrix struct {
	sourcePath              string // 路径
	targetPath              string // 目标路径
	ext                     string // 扩展名
	cols                    int    // 列数
	rows                    int    // 行数
	logicWidth, logicHeight int    // 逻辑大图尺寸
	blockWidth, blockHeight int    // 单图尺寸
	levels                  int    // 地图级别
}

var Matrix = new(matrix)

/*
获取矩阵信息
	params:
		matrix_path: 当前工作的路径
	returns:
		cols: 列
		rows: 行
		err: error message
*/
func (o *matrix) GetMatrix(matrix_path string) (cols, rows int, err error) {
	files, err := ioex.ListFiles(matrix_path, o.ext)
	if err != nil {
		return 0, 0, err
	}
	if len(files) == 0 {
		return 0, 0, nil
	}
	for _, name := range files {
		name = strings.Replace(name, path.Ext(name), "", -1)
		name = strings.Replace(name, "-", "_", -1)
		data := strings.Split(name, "_")
		col, err := strconv.ParseFloat(data[0], 64)
		if err != nil {
			return 0, 0, err
		}
		row, err := strconv.ParseFloat(data[1], 64)
		if err != nil {
			return 0, 0, err
		}
		cols = int(math.Max(col, float64(cols)))
		rows = int(math.Max(row, float64(rows)))
	}
	cols += 1
	rows += 1
	return cols, rows, nil
}

func (o *matrix) ready() (err error) {

	log.Println("ready")

	level := o.levels
	for level > 0 {

		currlevelPath := path.Join(o.targetPath, strconv.Itoa(level))
		highLevelPath := path.Join(o.targetPath, strconv.Itoa(level+1))

		log.Println("ready", currlevelPath)

		// 最大级别直接 copy 原始图片
		if level == o.levels {
			if err := ioex.CopyFolderFiles(o.sourcePath, currlevelPath); err != nil {
				return err
			}
		}

		// 非最大级别
		if level < o.levels {

			// 缩小图像
			files, err := ioex.ListFiles(highLevelPath, "")
			if err != nil {
				return err
			}
			if len(files) > 1 {
				width, height, err := images.Info(path.Join(highLevelPath, "0_0"+o.ext))
				if err != nil {
					return err
				}
				if width%2 == 0 && height%2 == 0 {
					var wg sync.WaitGroup
					wg.Add(len(files))
					for _, name := range files {
						go func(highLevelPath, currlevelPath, name string) {
							images.Resize(path.Join(highLevelPath, name), path.Join(currlevelPath, name), 0.5)
							wg.Done()
						}(highLevelPath, currlevelPath, name)
					}
					wg.Wait()
				} else {
					log.Println("Join", currlevelPath)
					if err := o.Join(highLevelPath, currlevelPath); err != nil {
						return err
					}
					currLevelFile := path.Join(currlevelPath, "0_0"+o.ext)
					if err := images.Resize(currLevelFile, currLevelFile, 0.5); err != nil {
						return err
					}
				}
			}

			// 高级别只有一个图片文件
			if len(files) == 1 {
				images.Resize(path.Join(highLevelPath, "0_0"+o.ext), path.Join(currlevelPath, "0_0"+o.ext), 0.5)
			}

		}

		level--
	}
	return nil
}

/*
矩形拼接
*/
func (o *matrix) Join(source_path, target_path string) error {

	log.Println("ext:", o.ext)

	os.MkdirAll(target_path, 0755)

	if o.ext == "" {
		ext, err := CheckExtension(source_path)
		if err != nil {
			log.Println(err.Error())
			return err
		}
		o.ext = ext
	}
	log.Println("ext:", o.ext)

	width, height, err := images.Info(path.Join(source_path, "0_0"+o.ext))
	if err != nil {
		log.Println(err.Error())
		return err
	}
	log.Println(width, height)
	cols, rows, err := o.GetMatrix(source_path)
	if err != nil {
		return err
	}
	o.cols = cols
	o.rows = rows
	width *= o.cols
	height *= o.rows
	log.Println(width, height)
	dst := imaging.New(width, height, color.NRGBA{0, 0, 0, 0})
	left := 0
	for col := 0; col < o.cols; col++ {
		top := 0
		var w, h int
		for row := 0; row < o.rows; row++ {
			image_path := fmt.Sprintf("%s/%d_%d%s", source_path, col, row, o.ext)
			log.Println(image_path)
			img, err := imaging.Open(image_path)
			if err != nil {
				log.Println(err)
				// return err
				break
			}
			bounds := img.Bounds()
			h = bounds.Max.Y
			w = bounds.Max.X
			dst = imaging.Paste(dst, img, image.Pt(left, top))
			top += h
		}
		left += w
	}
	return imaging.Save(dst, fmt.Sprintf("%s/0_0%s", target_path, o.ext))

}

func (o *matrix) cut() error {

	// create target folder
	if _, err := os.Stat(o.targetPath); os.IsNotExist(err) {
		_ = os.Mkdir(o.targetPath, 0755)
	}

	for level := o.levels; level > 0; level-- {
		levelPath := fmt.Sprintf("%s/%d", o.targetPath, level)
		if _, err := os.Stat(levelPath); os.IsNotExist(err) {
			// 必须分成两步：先创建文件夹、再修改权限
			_ = os.Mkdir(levelPath, 0755)
		}
		targetPath := fmt.Sprintf("%s/%d", o.targetPath, level)
		files, _ := ioex.ListFiles(targetPath, "")
		if len(files) == 1 {
			img, err := images.Load(fmt.Sprintf("%s/%d/0_0%s", o.targetPath, level, o.ext))
			if err != nil {
				return err
			}
			Cut(img, fmt.Sprintf("%s/%d", o.targetPath, level), o.ext)
		} else {
			Carpet.Run(levelPath, levelPath)
		}
	}
	return nil
}

/*
连续视野瓦片制作
	flow:
		1. 清理目标文件夹
		2. 检查图片类型
		3. 获取图片尺寸 (单图)
		4. 获取逻辑大图尺寸 (包含检测每张图的大小是否一致)
		5. 计算列、行 (逻辑大图尺寸 / 单图尺寸)
		6. 计算最大级别
		7. 生成瓦片 (2048按照除8处理、非方形采用地毯式处理)
	params:
		source: 源文件路径
		target: 目标文件路径
	returns:
		err: error message
*/
func (o *matrix) Run(source, target string) (err error) {

	o.sourcePath = source
	o.targetPath = target

	// 清理目标文件夹
	os.RemoveAll(target)

	// 检查图片类型
	ext, err := CheckExtension(o.sourcePath)
	if err != nil {
		return err
	}
	o.ext = ext

	// 单图尺寸
	blockHeight, blockWidth, err := GetBlockSize(o.sourcePath, o.ext)
	if err != nil {
		return
	}
	o.blockHeight = blockHeight
	o.blockWidth = blockWidth

	// 矩形尺寸
	cols, rows, err := o.GetMatrix(o.sourcePath)
	if err != nil {
		return
	}
	o.cols = cols
	o.rows = rows

	// 逻辑大图尺寸
	logicWidth, logicHeight := cols*blockWidth, rows*blockHeight
	o.logicHeight = logicHeight
	o.logicWidth = logicWidth

	log.Println(logicWidth, logicHeight)

	// 瓦片最大级别
	o.levels = GetMaxLevel(logicWidth, logicHeight)

	if e := o.ready(); e != nil {
		return e
	}

	if e := o.cut(); e != nil {
		return e
	}

	return nil
}
