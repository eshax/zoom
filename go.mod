module gitee.com/eshax/zoom

go 1.16

require (
	gitee.com/eshax/images v0.2.0
	gitee.com/eshax/ioex v0.4.0
	github.com/disintegration/imaging v1.6.2
)
