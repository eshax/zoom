# zoom

##### Description

瓦片化图像处理

##### Installation

```
go get gitee.com/eshax/zoom
```

##### Usage

- go.mod
```
module zoomtest

go 1.16

require gitee.com/eshax/zoom v1.2.12
```

- go.sum
```
gitee.com/eshax/zoom v1.2.4 h1:r1dJ095pfhAoiqIrZkv+OcuPU0ofCcOJBJdfdtLwBzM=
gitee.com/eshax/zoom v1.2.4/go.mod h1:RzvWQo3oqdkqgKtpL/Sg3+A2wDlk9TBjQU0zthcUipk=
github.com/disintegration/imaging v1.6.2 h1:w1LecBlG2Lnp8B3jk5zSuNqd7b4DXhcjwek1ei82L+c=
github.com/disintegration/imaging v1.6.2/go.mod h1:44/5580QXChDfwIclfc/PCwrr44amcmDAg8hxG0Ewe4=
golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 h1:hVwzHzIUGRjiF7EcUjqNxk3NCfkPxbDKRdnNE1Rpg0U=
golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8/go.mod h1:FeLwcggjj3mMvU+oOTbSwawSJRM1uh48EjtB4UJZlP0=
golang.org/x/text v0.3.0/go.mod h1:NqM8EUOU14njkJ3fqMW+pc6Ldnwhi/IjpwHt7yyuwOQ=
```

- main.go
```
package main

import (
    "os"
    "gitee.com/eshax/zoom"
)

func main() {
    zoom.Matrix.Run("100x", "dzi")
}
```