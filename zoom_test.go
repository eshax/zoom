package zoom

import (
	"fmt"
	"log"
	"os"
	"testing"

	"gitee.com/eshax/images"
)

func TestGetLevelSize(t *testing.T) {
	w, h, err := GetLevelSize(2048, 2448, 11)
	if err != nil {
		t.Error(err)
	}
	log.Println(w, h)
}

func TestGetMaxLevel(t *testing.T) {
	maxlevel := GetMaxLevel(2048, 2448)
	t.Log(maxlevel)
}

func TestTile(t *testing.T) {
	for i := 0; i < 10; i++ {
		log.Println("文件切图")
		img, err := images.Load("dist/html/images/0_0.jpg")
		if err != nil {
			t.Error(err)
		}
		levels := GetMaxLevel(img.Bounds().Max.X, img.Bounds().Max.Y)
		for level := levels; level > 0; level-- {
			if level < levels {
				img = images.ResizeImage(img, 0.5)
			}
			imgs, err := Tile(img)
			if err != nil {
				t.Error(err)
			}
			for col := range imgs {
				for row := range imgs[col] {
					os.MkdirAll(fmt.Sprintf("dist/html/images/0_0/%d", level), 0755)
					if err := images.Save(fmt.Sprintf("dist/html/images/0_0/%d/%d_%d.jpg", level, col, row), imgs[col][row]); err != nil {
						log.Println(err)
					}
				}
			}
		}
	}
	log.Println()
}
