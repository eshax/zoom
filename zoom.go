package zoom

import (
	"errors"
	"fmt"
	"image"
	"log"
	"math"
	"os"
	"path"

	"gitee.com/eshax/images"
	"gitee.com/eshax/ioex"
	"github.com/disintegration/imaging"

	_ "image/gif"
)

var extensions = [2]string{".jpg", ".png"} // 有效图片扩展名
var tileSize = 256

/*
获取原始块尺寸
	params:
		source: 源文件或路径
*/
func GetBlockSize(source, ext string) (height, width int, err error) {

	pi, err := os.Stat(source)
	if err != nil {
		return
	}

	file_path := source

	if pi.IsDir() {
		file_path = path.Join(source, "0_0"+ext)
	}

	width, height, err = images.Info(file_path)
	if err != nil {
		return
	}

	return
}

/*
单图瓦片制作
*/
func Cut(img image.Image, target, ext string) (err error) {

	width := img.Bounds().Max.X
	height := img.Bounds().Max.Y

	log.Println("width:", width, "height:", height)

	if width > tileSize || height > tileSize {
		var left, top, col, row = 0, 0, 0, 0
		for left < width {
			w := int(math.Min(float64(width-left), float64(tileSize)))
			for top < height {
				h := int(math.Min(float64(height-top), float64(tileSize)))
				tile := imaging.Crop(img, image.Rect(left, top, left+w, top+h))
				if err := imaging.Save(tile, fmt.Sprintf("%s/%d_%d%s", target, col, row, ext)); err != nil {
					return err
				}
				top += h
				row += 1
			}
			left += w
			col += 1
			top = 0
			row = 0
		}
	} else {
		if err := imaging.Save(img, fmt.Sprintf("%s/0_0%s", target, ext)); err != nil {
			return err
		}
	}

	return nil
}

func Tile(img image.Image) (imgs [][]image.Image, err error) {

	width := img.Bounds().Max.X
	height := img.Bounds().Max.Y

	// log.Println("width:", width, "height:", height)

	cols := width / tileSize
	rows := height / tileSize

	if (width % tileSize) > 0 {
		cols += 1
	}

	if (height % tileSize) > 0 {
		rows += 1
	}

	// log.Println(cols, rows)

	imgs = make([][]image.Image, cols)
	for c := range imgs {
		imgs[c] = make([]image.Image, rows)
	}

	for col := 0; col < cols; col++ {
		left := col * tileSize
		for row := 0; row < rows; row++ {
			top := row * tileSize
			tile := imaging.Crop(img, image.Rect(left, top, left+tileSize, top+tileSize))
			imgs[col][row] = tile
		}
	}

	return
}

/*
计算瓦片的最大级别
	params:
		width: 图像宽度
		height: 图像高度
	returns:
		max_level: 瓦片的最大级别
*/
func GetMaxLevel(width, height int) (max_level int) {
	return int(math.Ceil(math.Log2(math.Max(float64(height), float64(width)))))
}

/*
计算某个级别的逻辑大图尺寸
*/
func GetLevelSize(width, height, level int) (levelWidth, levelHeight int, err error) {

	levelHeight = height
	levelWidth = width

	if level < 1 || level > GetMaxLevel(width, height) {
		err = errors.New("error level")
		return
	}

	for i := GetMaxLevel(width, height); i > level; i-- {
		levelHeight /= 2
		levelWidth /= 2
	}

	return
}

/*
检查图片类型
	params:
		source_path: 源文件路径
	returns:
		extension: 图片扩展名
		err: error message
*/
func CheckExtension(srcPath string) (ext string, err error) {
	exts, err := ioex.ListExtensions(srcPath)
	if err != nil {
		log.Println(err.Error())
		return
	}
	if len(exts) != 1 {
		err = errors.New("异常的图片类型")
		log.Println(err.Error())
		return
	}
	b := false
	for _, ext := range extensions {
		if ext == exts[0] {
			b = true
			continue
		}
	}
	if !b {
		err = errors.New("存在不能处理的图片类型")
		log.Println(err.Error())
		return
	}
	ext = exts[0]
	return
}
